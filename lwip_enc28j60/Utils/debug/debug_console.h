#ifndef __DEBUG_CONSOLE_H_
#define __DEBUG_CONSOLE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>

// Utils
#define __check_cmd_at_pos(x,p) (strncmp(&result[p], x, strlen(x)) == 0)
#define __check_cmd(x) __check_cmd_at_pos(x,0)
#define __param_pos(x)	((char *)(&result[strlen(x)]))

#define console_available()	UART_DMA_RX_Available(UART_DEBUG)
	
int console_read(char **result);

#ifdef __cplusplus
}
#endif

#endif

#include <stdint.h>
#include "uart/uart_services.h"
#include "bsp.h"
#include "debug_console.h"

#define CONSOLE_BUFFER		512
static char consoleBuffer[CONSOLE_BUFFER];

void console_flush(void)
{
	UART_DMA_RX_Flush(UART_DEBUG);
}

int console_read(char **result)
{
  static int i = 0;
	
  while (console_available())
  {
    uint8_t inChar;
    uint32_t len = 1;
		UART_DMA_RX_Get(UART_DEBUG, &inChar);
    if (inChar == '\r')
    {
      consoleBuffer[i] = '\0';
			len = i;
			i = 0;
			*result = consoleBuffer;
			// console_flush();
      return len;
    }
    else if (inChar != '\n'){
      consoleBuffer[i] = inChar;
      i++;
			if (i == (CONSOLE_BUFFER-1)){
				consoleBuffer[i] = '\0';
				len = i;
				i = 0;
				*result = consoleBuffer;
				// console_flush();
				return len;
			}
    }
  }
	return 0;
}

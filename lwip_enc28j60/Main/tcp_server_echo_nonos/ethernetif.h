#ifndef __ETHERNETIF_H__
#define __ETHERNETIF_H__


#include "lwip/err.h"
#include "lwip/netif.h"
#include "target.h"
#include "enc28j60.h"

/* Exported types ------------------------------------------------------------*/
err_t ethernetif_init(struct netif *netif);
void ethernetif_input(struct netif *netif);
void ethernetif_set_link(struct netif *netif);
void ethernetif_update_config(struct netif *netif);
void ethernetif_notify_conn_changed(struct netif *netif);

/* Export macros ----------------------------- */
#define ethernetif_isLinkChanged()	ENC28J60_isLinkChanged()
#define ethernetif_isPacket()	ENC28J60_isPacket()

extern bool ethernetif_int_request;
extern struct netif gnetif;

#endif


/*
	EthernetIf LL for ENC28J60

	HieuNT
	16/8/2017
*/
#define __DEBUG__ 0
#ifndef __MODULE__
	#define __MODULE__ "enc28j60_spi"
#endif
#include "debug/debug.h"
#include "debug/debug_console.h"

#include "bsp.h"
#include "enc28j60.h"

/* ================= Function Implementation ================== */
__IO bool spi2Done;
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if (hspi == &hspi2){
		spi2Done = true;
	}
}

void ethPorting_disableChip(void)
{
	HAL_GPIO_WritePin(ENC_CS_GPIO_Port, ENC_CS_Pin, GPIO_PIN_SET); 
	HAL_Delay(2);
}

void ethPorting_enableChip(void)
{
	HAL_GPIO_WritePin(ENC_CS_GPIO_Port, ENC_CS_Pin, GPIO_PIN_RESET); 
	HAL_Delay(2);
}

void ethPorting_SPI_Init(void)
{
	// Do nothing
}

unsigned char ethPorting_SPI_SendByte(unsigned char dt)
{
	uint8_t txByte = dt;
	uint8_t rxByte = 0;
	// HAL_SPI_TransmitReceive(&hspi2, &txByte, &rxByte, 1, ETHPORTING_SPI_TIMEOUT);
	spi2Done = false;
	HAL_SPI_TransmitReceive_DMA(&hspi2, &txByte, &rxByte, 1);
	while(!spi2Done);
	DEBUG("SendByte: %02X, %02X\n", dt, rxByte);
	return rxByte;
}

void ethPorting_SPI_SendBytes(unsigned char *tx, unsigned char *rx, unsigned int len)
{
	// HAL_SPI_TransmitReceive(&hspi2, tx, rx, len, ETHPORTING_SPI_TIMEOUT * len);
	spi2Done = false;
	HAL_SPI_TransmitReceive_DMA(&hspi2, tx, rx, len);
	while(!spi2Done);
	DEBUG("SendBytes:\nTX:");
	for (int i = 0; i < len; ++i)
	{
		DEBUGX("%02X ", tx[i]);
	}
	DEBUGX("\nRX:");
	for (int i = 0; i < len; ++i)
	{
		DEBUGX("%02X ", rx[i]);
	}
	DEBUGX("\n");
}


/*
	Timer interrupt handler callback config

	HieuNT
	23/3/2017
*/

#include "target.h"

// Ham nay chi dung khi timer tick based tu system tick timer
/*
void HAL_SYSTICK_Callback(void)
{
	halTimerIrqCount++;
}
*/

/* ======================= HIEUNT NOTESSSSSSSSSSSSS ==================== */
/*
	Do SystemTick timer xai cho freertos tick ==> system tick timer base from timer1
	==> nhet HAL_TIM_PeriodElapsedCallback()
*/
/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
/* USER CODE BEGIN Callback 0 */

/* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
/* USER CODE BEGIN Callback 1 */

/* USER CODE END Callback 1 */
}

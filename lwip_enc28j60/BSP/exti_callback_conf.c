
/*
	External interrupt callback handler

	HieuNT
	30/7/2017
*/


#include "target.h"
#include "main.h"
#include "ethernetif.h"



void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  switch (GPIO_Pin){
    case ENC_INT_Pin:
    	ethernetif_int_request = true;
    	break;
  }
}



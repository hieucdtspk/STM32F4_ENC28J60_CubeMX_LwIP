// Microchip ENC28J60 Ethernet Interface Driver
// Author: Pascal Stang
// Modified by: Guido Socher
// Copyright: GPL V2
//
// This driver provides initialization and transmit/receive
// functions for the Microchip ENC28J60 10Mb Ethernet Controller and PHY.
// This chip is novel in that it is a full MAC+PHY interface all in a 28-pin
// chip, using an SPI interface to the host processor.
//
// 2010-05-20 <jc@wippler.nl>

// Last modified: HieuNT
// Porting to ANSI C syntax!

#ifndef ENC28J60_H
#define ENC28J60_H

#include <stdint.h>
#include <stdbool.h>

typedef uint8_t byte;
	
/** This class provide low-level interfacing with the ENC28J60 network interface. This is used by the EtherCard class and not intended for use by (normal) end users. */

// extern uint8_t buffer[]; //!< Data buffer (shared by recieve and transmit)
extern uint8_t *buffer; //!< Data buffer (shared by recieve and transmit)
extern uint16_t bufferSize; //!< Size of data buffer
extern bool broadcast_enabled; //!< True if broadcasts enabled (used to allow temporary disable of broadcast for DHCP or other internal functions)

//uint8_t* tcpOffset () { return buffer + 0x36; } //!< Pointer to the start of TCP payload

/* ================= Porting Prototypes ================== */
#define ETHPORTING_BUFFERSIZE  1500
#define ETHPORTING_SPI_TIMEOUT	2

void ethPorting_enableChip(void);
void ethPorting_disableChip(void);
void ethPorting_SPI_Init(void);
unsigned char ethPorting_SPI_SendByte(unsigned char dt);
void ethPorting_SPI_SendBytes(unsigned char *tx, unsigned char *rx, unsigned int len);

void ENC28J60_Cb_Register(	void (*pCb_initSPI)(void), \
							void (*pCb_enableChip)(void), \
							void (*pCb_disableChip)(void), \
							byte (*pCb_xferSPI)(byte data), \
							void (*pnxferSPI)(unsigned char *tx, unsigned char *rx, unsigned int len));


/**   @brief  Initialise SPI interface
*     @note   Configures Arduino pins as input / output, etc.
*/
//void ENC28J60_initSPI (void);

/**   @brief  Initialise network interface
*     @param  size Size of data buffer
*     @param  macaddr Pointer to 4 byte hardware (MAC) address
*     @param  csPin Arduino pin used for chip select (enable network interface SPI bus). Default = 8
*     @return <i>uint8_t</i> ENC28J60 firmware version or zero on failure.
*/
uint8_t ENC28J60_initialize (const uint16_t size, const uint8_t* macaddr);

// Determine Packet interrupt
bool ENC28J60_isPacket(void);
// Determine Link is Changed
bool ENC28J60_isLinkChanged(void);

/**   @brief  Check if network link is connected
*     @return <i>bool</i> True if link is up
*/
bool ENC28J60_isLinkUp (void);

#if 0 // Origin
/**   @brief  Sends data to network interface
*     @param  len Size of data to send
*     @note   Data buffer is shared by recieve and transmit functions
*/
void ENC28J60_packetSend (uint16_t len);
void ENC28J60_packetSend_pBuf (uint16_t len, uint8_t *pOutBuf);
#else // Replace with:
void ENC28J60_BeginPacketSend(uint16_t packetLength);
void ENC28J60_PacketSend(uint8_t *data,uint16_t length);
void ENC28J60_EndPacketSend(void);
#endif

#if 0 // Origin
// Get len of current packet!
uint16_t ENC28J60_packetReceive_GetLen(void);

/**   @brief  Copy recieved packets to data buffer
*     @return <i>uint16_t</i> Size of recieved data
*     @note   Data buffer is shared by recieve and transmit functions
*/
uint16_t ENC28J60_packetReceive (void);
uint16_t ENC28J60_packetReceive_pBuf (uint8_t *pInBuf);
#else // Replaced with:
uint16_t ENC28J60_BeginPacketReceive(void);
void ENC28J60_PacketReceive(uint8_t *data,uint16_t length);
void ENC28J60_EndPacketReceive(void);
#endif

/**   @brief  Copy data from ENC28J60 memory
*     @param  page Data page of memory
*     @param  data Pointer to buffer to copy data to
*/
void ENC28J60_copyout (uint8_t page, uint8_t* data);

/**   @brief  Copy data to ENC28J60 memory
*     @param  page Data page of memory
*     @param  data Pointer to buffer to copy data from
*/
void ENC28J60_copyin (uint8_t page, uint8_t* data);

/**   @brief  Get single byte of data from ENC28J60 memory
*     @param  page Data page of memory
*     @param  off Offset of data within page
*     @return Data value
*/
uint8_t ENC28J60_peekin (uint8_t page, uint8_t off);

/**   @brief  Put ENC28J60 in sleep mode
*/
void ENC28J60_powerDown(void);  // contrib by Alex M.

/**   @brief  Wake ENC28J60 from sleep mode
*/
void ENC28J60_powerUp(void);    // contrib by Alex M.

/**   @brief  Enable reception of broadcast messages
*     @param  temporary Set true to temporarily enable broadcast
*     @note   This will increase load on recieved data handling
*/
void ENC28J60_enableBroadcast(bool temporary); // default temporary = false

/**   @brief  Disable reception of broadcast messages
*     @param  temporary Set true to only disable if temporarily enabled
*     @note   This will reduce load on recieved data handling
*/
void ENC28J60_disableBroadcast(bool temporary); // default temporary = false

/**   @brief  Enables reception of mulitcast messages
*     @note   This will increase load on recieved data handling
*/
void ENC28J60_enableMulticast (void);

/**   @brief  Disable reception of mulitcast messages
*     @note   This will reduce load on recieved data handling
*/
void ENC28J60_disableMulticast(void);

/**   @brief  Reset and fully initialise ENC28J60
*     @param  csPin Arduino pin used for chip select (enable SPI bus)
*     @return <i>uint8_t</i> 0 on failure
*/
uint8_t ENC28J60_doBIST(void);

#endif

// BSP header

#ifndef __BSP_H_
#define __BSP_H_

#include "target.h"
#include "main.h"
#include "gpio.h"
#include "spi.h"
#include "usart.h"
#include "uart/uart_services.h"

#define ENC_RS(x) do {HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, x ? GPIO_PIN_RESET : GPIO_PIN_SET);} while(0)
#define ENC_CS(x) do {HAL_GPIO_WritePin(ENC_CS_GPIO_Port, ENC_CS_Pin, x ? GPIO_PIN_RESET : GPIO_PIN_SET);} while(0)
#define LED_ETH_LINKUP(x) do { HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, x ? GPIO_PIN_SET : GPIO_PIN_RESET);\
																HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, x ? GPIO_PIN_RESET : GPIO_PIN_SET);} while(0)
#define LED_ETH_NETIF(x) do { HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, x ? GPIO_PIN_SET : GPIO_PIN_RESET);\
																HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, x ? GPIO_PIN_RESET : GPIO_PIN_SET);} while(0)

#define UART_DEBUG (&huart2_dma)

#endif


/**
	Header file for uart services
	
	HieuNT
	23/7/2015
*/

#ifndef __UART_SERVICES_H_
#define __UART_SERVICES_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "usart.h"
#include "uart_dma_ringbuffer.h"
#include "uart_sw_ringbuffer.h"


#define UART2_TX_BUFFER_SIZE		512
#define UART2_RX_BUFFER_SIZE		512

// Add for missing from usart.h
extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart2_tx;

// Export usart dms/sw ringbuffer
extern UART_DMA_RINGBUFFER_T huart2_dma;

// Export functions
void UART_SERVICES_Init(void);
void UART_SERVICES_UnitTest(void);

#ifdef __cplusplus
 }
#endif
#endif

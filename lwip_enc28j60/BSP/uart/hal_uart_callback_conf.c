/*
  HAL UART CALLBACK HANDLER CONFIG
  
  HieuNT
  
  21/7/2016
*/

#include "uart/uart_services.h"
//#include "uart_mpcm_services.h"
//#include "hal_uart_callback_conf.h"

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart2){
		UART_DMA_TX_DMAComplete(&huart2_dma);
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart2){
		UART_DMA_RX_DMAComplete(&huart2_dma);
	}
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	// static uint32_t errCnt = 0;
	
	if (huart == &huart2){
		// errCnt++;
	}
}
